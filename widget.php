<?php
if (!isset($_GET['serverid'])) {
    header('Content-Type: image/png');
    $rect = imagettfbbox(11, 0, './Verdana.ttf', 'ERROR: Bitte ?serverid= an die URL hängen.');
    $minX = min(array($rect[0],$rect[2],$rect[4],$rect[6]));
    $maxX = max(array($rect[0],$rect[2],$rect[4],$rect[6]));
    $minY = min(array($rect[1],$rect[3],$rect[5],$rect[7]));
    $maxY = max(array($rect[1],$rect[3],$rect[5],$rect[7]));

    $image = imagecreate($maxX-$minX, $maxY-$minY);
    $white = imagecolorallocate($image, 255, 255, 255);
    $black = imagecolorallocate($image, 0, 0, 0);
    imagettftext($image, 11, 0, 0, $maxY-$minY-5, $black, './Verdana.ttf', 'ERROR: Bitte ?serverid= an die URL hängen.');
    imagepng($image);
    imagedestroy($image);
    exit();
}

header('Content-Type: image/svg+xml');

$opts = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
    ),
);
$context = stream_context_create($opts);

$address = 'https://discordapp.com/api/servers/'.$_GET['serverid'].'/widget.json';
$content = file_get_contents($address, false, $context, 0);
$serverinfo = json_decode($content, true);
$servername = $serverinfo['name'];
$servermembers = count($serverinfo['members']);

$background_color = '#7289da';
$servernamewidth = getTextWidthFromImage($servername);
$servermemberswidth = getTextWidthFromImage($servermembers);
$textwidth = $servernamewidth + $servermemberswidth;

$svgnamearea = $servernamewidth-20;
$svgnumbarea = $servermemberswidth+10;
$svgwidth = $svgnamearea + $svgnumbarea+11;

$svg = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink" width="'.$svgwidth.'" height="20">
    <linearGradient id="grads" x2="0" y2="100%">
        <stop offset="0" stop-color="#bbb" stop-opacity=".1"></stop>
        <stop offset="1" stop-opacity=".1"></stop>
    </linearGradient>
    <mask id="mask">
        <rect width="'.$svgwidth.'" height="20" rx="3" fill="#fff"></rect>
    </mask>
    <g mask="url(#mask)">
        <path fill="#555" d="M0 0h'.($svgnamearea+11).'v20H0z"></path>
        <path fill="#7289da" d="M'.($svgnamearea+11).' 0h'.($svgnumbarea).'v20h-'.($svgnumbarea).'z"></path>
        <path fill="url(#grads)" d="M0 0h'.$svgwidth.'v20H0z"></path>
    </g>
    <g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="11">
	<image xlink:href="https://discordapp.com/assets/41484d92c876f76b20c7f746221e8151.svg" x="5" y="6" width="11" height="11" />
	<image xlink:href="https://discordapp.com/assets/1c8a54f25d101bdc607cec7228247a9a.svg" x="5" y="5" width="11" height="11" />
        <text x="'.($svgnamearea/2+11).'" y="15" fill="#010101" fill-opacity=".3">'.$servername.'</text>
        <text x="'.($svgnamearea/2+11).'" y="14">'.$servername.'</text>
        <text x="'.($svgnumbarea/2+$svgnamearea+11).'" y="15" fill="#010101" fill-opacity=".3">'.$servermembers.'</text>
        <text x="'.($svgnumbarea/2+$svgnamearea+11).'" y="14">'.$servermembers.'</text>
    </g>
</svg>';
echo $svg;

function getTextWidthFromImage($text) {
    $tb = imagettfbbox(11, 0, "./Verdana.ttf", $text);
    return $tb[2];
}